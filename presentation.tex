\documentclass[a4paper, 8pt, fleqn]{beamer}

% \usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme[onlymath]{serif} % default family is serif
% \usepackage{fontspec}
% \setmainfont{Liberation Serif}
\makeatletter
\renewcommand\@makefntext[1]{\leftskip=0em\hskip0em\@makefnmark#1}
\makeatother

\usepackage{custom-beamer}

% \definecolor{MyBlue}{RGB}{83,121,170}
\setbeamerfont{title}{size=\Huge}
% \setbeamercolor{title}{fg=white}

\setbeamerfont{subtitle}{size=\huge}
% \setbeamercolor{subtitle}{fg=white}

% \setbeamercolor{author}{fg=white}

% \setbeamerfont{institute}{size=\normalsize}
% \setbeamercolor{institute}{fg=white}


\title{\textbf{Quantum Zeno Dynamics}}
\subtitle{Examples of coherent evolution\\ induced by strong interactions} 
\author[Carmelo Mordini]{\vspace{0pt}\\ \textbf{Carmelo Mordini}}
%%logo
% \logo{\includegraphics[scale=10]{pics/Logo.jpeg}}

% \institute[SNS]{Scuola Normale Superiore \\ University of Pisa}
\date{}


% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSubsection[]
\AtBeginSection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents%[currentsection,currentsubsection]
  \end{frame}
}

% Let's get started


\begin{document}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
% \setbeamertemplate{caption}{%
% 	\begin{beamercolorbox}[wd=.5\paperwidth, sep=.2ex]{block body}\insertcaption%
% 	\end{beamercolorbox}%
% 	}
%\beamertemplatenavigationsymbolsempty

\begin{frame}[plain]
\advance\textwidth2cm
\hsize\textwidth
\columnwidth\textwidth
  \titlepage
\end{frame}
% % 
% \begin{frame}{Outline}
% \transwipe[direction=270]
% \tableofcontents%[pausesections]% You might wish to add the option [pausesections]
% \end{frame}
% 
% \begin{frame}{STRUTTURA}
% 	\begin{itemize}
% 	    \item Intro e scopi
% 	    \item Teoria
% 	    \item Metodi
% 	    \item Risultati
% 	    \item Prospettive
% 	\end{itemize}
% \end{frame}

\section{The Quantum Zeno Effect}
\subsection{Overview}
\begin{frame}{The Zeno's paradox}

\emph{If everything when it occupies an equal space is at rest, and if that which is in locomotion is always occupying such a space at any moment, the flying arrow is therefore motionless.}\\
– as recounted by Aristotle, Physics VI:9, 239b5

\begin{columns}
\hfill
\fontsize{6pt}{7.2}\selectfont
    \begin{column}{.55\textwidth}
    \begin{block}{Zeno of Elea (ca. 490–430 BC)}
		Pre-Socratic Greek philosopher, \\member of Parmenides' Eleatic School.\\
		Became famous to the posterity for his long-discussed paradoxes.
    \end{block}

    \textbf{The arrow paradox} states that a flying arrow is never really moving.
    Indeed, at any given moment it occupies a space equals to its length,  from where it cannot move. Therefore, at every instant
    the arrow is motionless  the ``sum'' of these positions of rest is not a motion.
    \end{column}
	\begin{column}{.45\textwidth}	    
	\begin{figure}
		\flushright
	    \includegraphics[width=\textwidth]{pics/proof.png}
	\end{figure}
	\end{column}
\end{columns}
    
\end{frame}


\begin{frame}{The Quantum Zeno Effect}
	\begin{block}{}
		Frequent measurements on a system effectively slow the normal time evolution of a system. In the limit of \textbf{continuous measurements}, the system never leaves its initial state!
	\end{block}
	\vfill
	In the original formulation\footnote{Misra B. and Sudarshan E. C. G., J. Math. Phys. 18 756 (1977)}, the paradox relies on the ``continuous collapse'' of the wavefunction after the measurement, and on the quadratic behaviour of the survival probability at short times.
	\begin{align*}
	    &|\psi(t=0)\rangle = |\psi_0\rangle \\
		&p(t) = \left|\langle\psi_0| e^{-iHt} |\psi_0\rangle\right|^2 \simeq 1 - {t^2}/{\tau_Z^2} +o(t^2)\\
		&p^{(N)}(t) = \left[p(t/N)\right]^N \simeq \left[1 - {t^2}/{N^2\tau_Z^2}\right]^N \simeq \exp\left(- {t^2}/{N\tau_Z^2}\right)
			\xrightarrow{N\to \infty} 1
	\end{align*}
% 	Zeno’s quantum-mechanical arrow (the wavefunction), sped by the Hamiltonian, does not move if it is continuously observed.
	As the authors noticed, a careful monitoring of Schr\"{o}dinger's cat conditions could actually save his life!
\end{frame}

\begin{frame}{Quantum Zeno Dynamics}
Key ingredients:
	\begin{itemize}
	\item Measurements are instantaneous
	\item They can be repeated with infinitesimal time delay
	\item They are \textbf{selective}, which means that the state onto which to project is completely specified by the outcome
	\end{itemize}
    This can be generalized by introducing non-selective measurements like a ``yes-no question'' (e.g. particle decayed / particle undecayed)\\
	These are defined by dividing the Hilbert space $\mathcal{H}$ in (say) two subspaces, defined by the projector operators $P,\ Q=1-P$.
	\begin{align*}
		&\rho(t=0) = \rho_0 = P\rho_0 P\\
		&p^{(N)}(t) = \tr \left[V_N(t)\, \rho_0 V^\dagger_N(t)\right] \qquad \quad \text{where} \quad V_N(t) = [P\,e^{-iHt/N}P]^N
	\end{align*}
\end{frame}
\begin{frame}{Quantum Zeno Dynamics}
	In the $N\to\infty$ limit,
	\begin{equation*}
	\begin{cases}
	V_N(t) &\longrightarrow \quad U_Z(t) = P\,e^{-i\,PHPt}\\[1ex]
		\rho(t) &\longrightarrow \quad U_Z(t) \,\rho_0 U_Z^\dagger(t)\\[1ex]
			p^{(N)}(t) &\longrightarrow \quad \tr \left[ \rho(t) \right] = 1
		\end{cases}
		\end{equation*}

		\begin{itemize}
		    \item The system never leaves the $\mathcal{H}_P$ subspace, from where it started the time evolution
		    \item The dynamics in each subspace is unitary, and governed by an effective Zeno hamiltonian (the projection of $H$ onto $\mathcal{H}_P$)
		\end{itemize}
	It can be proven that for a continuous valued measurement, like the position in a definite region of space $S$, the QZD is identical to the normal quantum dynamics, but with the addition of a hard-wall potential on the boundary of $S$.
\end{frame}

\begin{frame}
In fact, Von Neumann's projective measurements are just a handy way to model a strong and complicated interaction of the system with the measurement apparatus, that in quantum mechanics has to be considered \emph{as a part of the physical world}(!).\\
\vspace{8pt}
QZD can be obtained as a consequence of purely unitary dynamics, in two alternative and equivalent approaches\footnote{Facchi P. and Pascazio S., J. Phys. A, 41 493001 (2008)}:
\begin{itemize}
    \item \textbf{Unitary kicks}:\\
		evolving $\rho_0$ in time $t$ by $\left[U_{kick}\,e^{-iHt/N}\right]^N$\\
		as $N\to\infty$ the Zeno subspaces are defined by the eigenspaces of $U_{kick}$
	\item \textbf{Continuous strong coupling}\footnote{My idea for the proof: pass to interaction representation, and expand in $1/K$ with a dual Dyson series. The zero-order must show QZD, and the successive terms can account for finite $K$ corrections.}:\\
		evolving $\rho_0$ with the hamiltonian $H_K = H + K\,H_c$\\
		as the coupling constant $K\to \infty$, the state is confined into the eigenspaces of $H_c$
\end{itemize}

% The effect stems in fact from the pure quanto-mechanical considerations that the interaction with an environment inevitably perturbs the system, entangling it with the outside, and leaves a reduced coherence within its parts once the environment is traced out.
\end{frame}

\section{Two examples}
\subsection{Realization of discrete-levels QZD in a Rb BEC}
\begin{frame}
\begin{figure}
        \flushleft 
        \includegraphics[width=\textwidth]{pics/Smerzi2013.pdf}
    \end{figure}
\begin{columns}
\hfill
% \fontsize{6pt}{7.2}\selectfont
\begin{column}{.55\textwidth}
    The system:\\
	$^{87}$Rb BEC on an atom-chip magnetic \si{\micro}trap\\
	\SI{8e4}{atoms} @$T_c \sim \SI{500}{nK}$\\
	\vspace{8pt}
	QZD induced in internal state space
	\end{column}
	\begin{column}{.45\textwidth}	    
	\begin{figure}
	\flushright
	\includegraphics[width=\textwidth]{pics/trap.pdf}
	\end{figure}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Experimental procedure}
    \begin{itemize}
        \item Sample prepared in $|2, 2\rangle$ hyperfine ground level
        \item A RF field couples all the five $F=2$ states with Rabi frequency $\Omega$
        \item Raman beams are used to selectively couple only the $|2,0\rangle$ with $|1,0\rangle$, while Dissipative light ($F=1 \to F'=2$) is used to probe the $|1,0\rangle$ population.
		\item A Stern--Gerlach method (expansion in a magnetic field gradient) is used to probe the populations in the $F=2$ states.
    \end{itemize}
	\begin{figure}
	    \flushleft
	    \includegraphics[width=.6\textwidth]{pics/subspaces.pdf}
	\end{figure}

\end{frame}

\begin{frame}{Experimental procedure}
    \begin{columns}
        \begin{column}{.35\textwidth}
            \begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/stern-gerlach-1.pdf}
			\end{figure}
        \end{column}
        \hfill
        \begin{column}{.55\textwidth}
			\begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/coupling1.pdf}
			\end{figure}
			$\pi/2$ RF pulse without Raman beams
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Experimental procedure}
    \begin{columns}
        \begin{column}{.35\textwidth}
            \begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/stern-gerlach-2.pdf}
			\end{figure}
        \end{column}
        \hfill
        \begin{column}{.55\textwidth}
			\begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/coupling2.pdf}
			\end{figure}
			$\pi/2$ RF pulse\\
			with always on Raman beams
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    The effects of QZD are probed with four different interaction protocols.
    Rabi oscillations restricted to the Zeno subspace are observed each time
    \begin{columns}
        \begin{column}{.4\textwidth}
            \begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/Rabi.pdf}
			\end{figure}
        \end{column}
        \hfill
        \begin{column}{.45\textwidth}
			\begin{figure}[t!]
			    \includegraphics[width=\textwidth]{pics/timepulses.pdf}
			\end{figure}
		Coherent dynamics is proven by a Ramsey interferometry scheme\\
		($\pi/2$ pulse -- delay time $T$ -- $\pi/2$ pulse)
			\begin{figure}[t!]
				\includegraphics[width=\textwidth]{pics/ramsey.pdf}
			\end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
\begin{block}{Recap}
     \begin{itemize}
		\item The equivalence of PVMs -- Unitary kicks -- Continuous coupling schemes is evident
        \item ``Measuring'' is not strictly necessary: the Zeno effect stems from the strong interaction alone\\
			(it is most clear in the continuous strong coupling regime)
    \end{itemize}
\end{block}
\end{frame}

\subsection{Dissipation-induced strong correlations in a many-body system}
\begin{frame}
\begin{figure}
\flushleft 
\includegraphics[width=\textwidth]{pics/Syassen2008.pdf}
\end{figure}
\begin{columns}
\hfill
\begin{column}{.55\textwidth}
% \fontsize{6pt}{7.2}\selectfont
The system:\\
	1D BEC of $^{87}$Rb Feschbach dimers\\
	condensed in a 2D optical lattice\\
	tight transverse confinement,\\
	free/periodic longitudinal potential\\
	\vspace{8pt}
	QZD manifests in reduced loss rates due to strong correlations.
	\end{column}
	\begin{column}{.45\textwidth}	    
		\begin{figure}
		\flushright
		\includegraphics[width=\textwidth]{pics/lattice.png}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Experimental procedure}
    \begin{itemize}
		\item Cold atoms loaded in a 3D lattice in Mott insulator regime @ filling = 2
		\begin{equation*}
			V(\vec x) = - V_\parallel\left[\cos^2(kx) + \cos^2(ky)\right] - V_\perp \cos^2(kz)
		\end{equation*}
		\item A homogeneous $\vec B$ field is ramped through a Feschbach resonance: adiabatic passage from atom pairs to \textbf{molecules}
		\item $V_\parallel$ is ramped to the final value ($0 \div 0.1 V_\perp$) and the particles are let interact, then dissociated and imaged in TOF.
    \end{itemize}
	
	\begin{columns}
	\hfill
	\begin{column}{.3\textwidth}
	\begin{figure}
	    \flushleft
	    \includegraphics[width=\textwidth]{pics/Feschbach.pdf}
	\end{figure}
	\end{column}
	\hfill
	\begin{column}{.65\textwidth}	    
		Molecules undergo \textbf{inelastic} collisions, modelled by a contact potential $g\delta(r)$ with complex $g$.\\
% 		The 1D coupling $g$ is related to the 3D scatt. length $a$ by\footnotemark[1]
% 		\begin{equation*}
% 			g = \df{2\hbar^2 a}{ma_\perp^2\left(1 + \frac{\zeta(000)a}{\sqrt 2 a_\perp}\right)}
% 		\end{equation*}
		\begin{block}{Tonks--Girardeau Gas (TGG)}
			At high $|g|$, the system is strongly correlated and the particles do not overlap $\Longrightarrow$ map to a system of 1D fermions
		\end{block}
		This prevents inelastic contact interaction and reduces losses.
		\end{column}
	\end{columns}
	\footnotetext[1]{Olshanii M., Phys. Rev. Lett. 81 (1998)}
\end{frame}

\begin{frame}{Free TGG gas: $V_\parallel = 0$}
The lossy system can be described by a master equation approach (part of the dissipation is included in the ``hamiltonian'')
\begin{equation*}
    i\hbar \partial_t \rho = H\rho - \rho H^\dagger + i \mathcal{D}[\rho]
\end{equation*}

With $V_\parallel = 0$, the appropriate model for free 1D bosons is the \emph{Lieb--Liniger} one:
\begin{equation*}
	\begin{cases}
		H = -\displaystyle\df{\hbar^2}{2m}\sum_i \df{\partial}{\partial x_i} + g \sum_{i<j} \delta(x_i - x_j)\\[1ex]
		\displaystyle\mathcal{D}[\rho] = - \Im g \int dx\ \hat\psi^2\, \rho\, \hat\psi^{\dagger\,2}
	\end{cases}
\end{equation*}
which leads to the density time evolution
\begin{equation*}
	\df{dn}{dt} = -\df{2\Im g}{\hbar} \gpair n^2 \qquad\qquad \text{where}\quad \gpair = \df{\langle n^2\rangle}{\langle n \rangle^2}
\end{equation*}
\end{frame}

\begin{frame}{Free TGG gas}
For the TGG ground state, with strong dimensionless interaction $\gamma = mg/\hbar^2 n \ (|\gamma| \gg 1)$
\begin{align*}
    &\gpair = \df{4\pi}{3|\gamma|^2} \propto n^2\\[1ex]
    &\df{dn}{dt} = -\chi n^4 \quad\Longrightarrow\quad N(t) = \df{N(0)}{\left(1 + \chi n(0)^3\,t\right)^{1/3}}
\end{align*}
\begin{columns}
	\hfill
	\begin{column}{.4\textwidth}	    
		If correlations were absent, $\gpair = 1$ and $\dot n \propto n^2$\\
		The number of atoms would decay much faster!\\
		\vspace{8pt}
		The strong inelastic scattering is basically a continuous strong coupling with the outside of the trap, and in the QZD point of view is acting as a hard-sphere potential between the particles.
	\end{column}
	\hfill
	\begin{column}{.55\textwidth}
	\begin{figure}
	    \flushleft
	    \includegraphics[width=\textwidth]{pics/V-perp-zero.pdf}
	\end{figure}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}{TGG in a lattice: $V_\parallel > 0$}
With $V_\parallel > 0$, we switch to a \emph{Bose--Hubbard} model:
	\begin{equation*}
	\begin{cases}
	H = \displaystyle -J \sum_k \left(a_k^\dagger a_{k+1} + \text{h.c.}\right) + \df U 2 \sum_k a_k^{\dagger\,2} a_k^2 \\[1ex]
	\displaystyle\mathcal{D}[\rho] = - \Im U \sum_k a_k^2 \rho a_k^{\dagger\,2} 
	\end{cases}
	\end{equation*}
	where $J$ and $U$ can be expressed in terms of $g(a)$ and $V_\parallel$.\\
	This leads to the time evolution for the density on the site $k$: $n_k = a_k^\dagger a_k$
	\begin{equation*}
	\df{d\langle n_k\rangle}{dt} = -\Gamma \gpair \langle n_k\rangle^2 \equiv -\kappa \langle n_k\rangle^2\qquad\qquad \text{where}\quad \gpair = \df{\langle n_k(n_k-1)\rangle}{\langle n_k \rangle^2}
	\end{equation*}
	In the limit of strong dissipation (encoded in $\Im U$) the average $n_k$ is limited to 1: $\gpair$ does not depend on the density.
\end{frame}

\begin{frame}{Effective loss rate}
    The effective loss rate is due to the interplay between the lattice barrier, tunneling, and the on-site inelastic scattering
    \begin{columns}
	\hfill
	\begin{column}{.4\textwidth}	    
		\begin{figure}
	    \flushleft
	    \includegraphics[width=\textwidth]{pics/livelli.pdf}
	\end{figure}
	\end{column}
	\hfill
	\begin{column}{.55\textwidth}
		\begin{equation*}
		    \begin{cases}
		        \Omega = \sqrt 8 J/\hbar\\
				\Delta = \Re U /\hbar\\
				\Gamma = -2\Im U /\hbar
		    \end{cases}
		\end{equation*}
	Neighbouring particles are lost with an effective rate
	\begin{equation*}
		\Gamma_{eff} = \df{\Omega^2}{\Gamma}\,\df{1}{1+\frac{2\Delta}{\Gamma}}
	\end{equation*}
	Note: as $\Gamma \gg \Omega \Longrightarrow \Gamma_{eff} \ll \Omega$
	\end{column}
	\end{columns}
	\vspace{8pt}
\underline{Strong dissipation is suppressing the tunneling between adjacent sites.}\\
A count of the probability to find each site occupied by one or two particles leads to the expression
\begin{equation*}
	\gpair = 2\,\df{q_{11}(\Gamma_{eff}/\Gamma)}{p_1^2} \simeq 2\,\df{(2p_1^2)(\Gamma_{eff}/\Gamma)}{p_1^2} = \df{4\Gamma_{eff}}{\Gamma}
\end{equation*}
\end{frame}


\begin{frame}{TGG in a lattice}
\begin{columns}
    \begin{column}{.6\textwidth}
			\begin{align*}
			&\gpair = \df{4\Gamma_{{eff}}}{\Gamma} \Longrightarrow \kappa = 4 \Gamma_{eff}\\[1ex]
			&\df{dn}{dt} = -\kappa n^2 \quad\Longrightarrow\quad N(t) = \df{N(0)}{1 + \kappa t}
	\end{align*}
    \end{column}
    \hfill
    \begin{column}{.4\textwidth}
		Varying $V_\parallel$ is possible to see the lattice contribution to the reduction of particle loss.
    \end{column}
\end{columns}


\begin{columns}
	\hfill
	\begin{column}{.5\textwidth}
	\begin{figure}
	    \flushleft
	    \includegraphics[width=\textwidth]{pics/V-perp-nonzero.pdf}
	\end{figure}
	\end{column}
	\hfill
	\begin{column}{.4\textwidth}	    
	\begin{figure}
	\flushleft
	\includegraphics[width=\textwidth]{pics/kappa-and-g2-vs-perp.pdf}
	\end{figure}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Conclusions}
	\begin{itemize}
	    \item We have seen the Quantum Zeno effect as a way to confine the time evolution  of a system in a defined subspace;
	    \item The strong coupling can be of dissipative nature (uncontrolled), leading to the curious phenomenon of a self-suppressed loss probability(!); 
	    \item It can as well be engineered with controllable sources, like the Raman beams: this opens fascinating perspectives in the field of quantum information, where preserving the coherence during computations is of fundamental importance.
	    
	\end{itemize}

\end{frame}

\end{document}



