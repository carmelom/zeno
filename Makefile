
MAIN            = presentation
MAIN_TEX        = $(MAIN).tex
MAIN_PDF        = $(MAIN).pdf
BIBLIOGRAFIA    = Bibliographypres.bib

EXT_CLEAN      = *.aux *.bbl *.blg *.brf *.idx \
                  *.ilg *.ind *.log *.lof *.lot *.toc *.out\
                  *.nav *.snm
EXT_DISTCLEAN  = *.pdf


PDFLATEX = TEXINPUTS=.//: pdflatex 

.PHONY : pdf distclean clean 

pdf : $(MAIN_PDF)

$(MAIN_PDF): $(MAIN_TEX) $(BIBLIOGRAFIA) custom-beamer.sty
	$(PDFLATEX) $(MAIN_TEX)
# 	bibtex $(MAIN)
# 	$(PDFLATEX) $(MAIN_TEX)
# 	$(PDFLATEX) $(MAIN_TEX)
	

distclean : clean
	rm -f $(EXT_DISTCLEAN)
# 	for f in $(EXT_DISTCLEAN); do 	\
# 		rm -f $(CHAP_FOLDER)/$$f ; 	\
# 		done
# 	for f in $(EXT_DISTCLEAN); do 	\
# 		rm -f $(APPX_FOLDER)/$$f ; 	\
# 		done
clean :
	rm -f $(EXT_CLEAN)
		
